## Introduction :

Bien que l'ELK et le Graylog soient communément comparés en tant que solutions de logs, il est important de faire une distinction importante. Graylog a été créé dès le début avec l'intention d'être une solution de journalisation alors que ELK est une solution de Big Data qui se trouve aussi être très bon en matière de journalisation. Cela peut sembler une distinction mineure, mais en réalité, elles sont assez importantes.



## Graylog

**Machine :**

RAM : 3 GB / 
CPU : 2 / 
Disque : HDD 15Go / 
OS : Debian

Paquet à installer : openjdk-8-jdk, mongodb-server, elasticsearch, graylog-server.

[Tutoriel](local/tuto-graylog.md)

**Avantages :**

- Facile à utiliser dès la sortie de l'emballage
- La fonctionnalité des flux est assez étonnante.
- Principalement basé sur l'interface graphique, donc la courbe d'apprentissage est beaucoup plus facile.
- Gère la plupart des détails de l'indexation Elasticsearch pour vous.
- Authentification dans la version gratuite
- Alerte dans la version gratuite

**Inconvéniants :**

- Le graphisme est basique et vous avez besoin d'utiliser Graphana et/ou Kibana en plus pour des besoins plus avancés.
- Le traitement des logs n'est pas aussi flexible que le Logstash.  Certaines installations plus importantes de Graylog traiteront les logs avec Logstash et les introduiront ensuite dans Graylog.
- Les extracteurs de logs personnalisés peuvent être utilisés et écrits, mais ils sont écrits en Java qui a une courbe d'apprentissage plus raide que Ruby (ce qui est utilisé pour Logstash).
- Il y a moins de souplesse dans la façon dont les index sont créés et écrits.
- Il y a beaucoup moins de plugins disponibles que dans Logstash et Kibana.

## ELK

**Machine :**

RAM : 3 GB / 
CPU : 2 / 
Disque : HDD 15Go / 
OS : Debian

Paquet à installer : openjdk-8-jdk, nginx, elasticsearch, logstash, kibana.

[Tutoriel](local/tuto-elk.md)

**Avantages :**

- Logstash est un pipeline de traitement de logs très puissant et personnalisable.
- Peut utiliser le même ensemble de produits et les mêmes connaissances pour les métriques et les logs et n'a pas besoin d'utiliser ou de prendre en charge plusieurs produits différents.
- Vous avez un contrôle total sur la façon dont vous indexation des données dans Elasticsearch.
- La configuration de Logstash est basée sur le texte. Copier/coller ces morceaux de configuration parce que vous faites généralement la même chose de 10 façons différentes.
- Les différents plugins beats sont livrés avec des tableaux de bord Kibana qui sont facilement importables.

**Inconvéniants :**

- La configuration de Logstash est basée sur le texte (courbe d'apprentissage)
- Logstash peut être un monopoliseur de ressources.
- Kibana est livré sans tableau de bord 'logging' par défaut.
- Vous êtes plus exposé à la gestion du groupe Elasticsearch et des indices.
- L'authentification est une fonction payante, ou vous devez trouver un plugin tiers.
- L'alerte est une fonction payante, ou vous devez trouver un plugin tiers.

## Conclusion

Si ce que vous recherchez, c'est un endroit pour stocker les logs et les rechercher selon vos besoins, ou si vous voulez une solution d'enregistrement mais que vous ne voulez pas et ne prévoyez pas y consacrer beaucoup de temps, alors Graylog est probablement un choix judicieux à examiner. Une fois installer,  il fonctionne assez bien et a des valeurs par défaut pour un grand nombre de paramètres qui peuvent vous causer/vous causeront du chagrin plus tard si vous les configurez mal.

Si vous prévoyez plutôt la nécessité de collecter des graphiques et des métriques ou si vous êtes simplement intéressé par de grandes données, il est probable que vous finirez par utiliser ELK. Pour une utilisation en entreprise de ELK, il est presque obligatoire d’adopter pour une licence pour bénéficier de toutes les fonctionnalités.

