# Solutions existantes
## [Syslog-ng](https://syslog-ng.com)

**Description :**

Syslog est une solution de gestion de logs open-source qui aide les ingénieurs et DevOps à collecter les données de logs à partir d'une grande variété de sources pour les traiter et éventuellement les envoyer vers un outil d'analyse de logs préféré. Avec Syslog, vous pouvez sans effort collecter, diminuer, catégoriser et corréler vos données de logs à partir de votre pile existante et les pousser vers l'avant pour analyse.

**Caractéristiques :**

- Open-source avec une large communauté d'adeptes.
- Évolutivité flexible, quelle que soit la taille de l'infrastructure.
- Prise en charge des plugins pour des fonctionnalités étendues.
- PatternDB pour trouver des modèles dans des journaux de données complexes.
- Les données peuvent être insérées dans des choix de bases de données communes.

**Prix :** Gratuit


## [RsysLog](https://www.rsyslog.com)

**Description :**

Rsyslog est un système ultra-rapide conçu pour le traitement des logs. Il offre d'excellents indices de performance, des caractéristiques de sécurité serrées et une conception modulaire pour des modifications personnalisées. Rsyslog s'est développé à partir d'un système de journalisation unique pour pouvoir analyser et trier les logs à partir d'une gamme étendue de sources, qu'il peut ensuite transformer et fournir une sortie à utiliser dans un logiciel dédié à l'analyse des logs.

**Caractéristiques :**

- Facile à implémenter dans des hôtes web communs.
- Vous permet de créer des méthodes d'analyse personnalisées.
- Générateur de configuration en ligne.
- Développement sur mesure disponible à la location.

**Prix :** Gratuit


## [Nagios](https://www.nagios.com/products/nagios-log-server/)
**Description :**

Nagios fournit une solution complète de gestion et de surveillance des logs qui est basée sur sa plate-forme Nagios Log Server. Avec Nagios, un outil d'analyse de logs leader sur ce marché, vous pouvez augmenter la sécurité de tous vos systèmes, comprendre votre infrastructure réseau et ses événements, et avoir accès à des données claires sur les performances de votre réseau et comment il peut être stabilisé.

**Caractéristiques :**

- Un puissant tableau de bord prêt à l'emploi qui donne aux clients un moyen de filtrer, de rechercher et d'effectuer une analyse complète de toutes les données des journaux entrants.
- Disponibilité étendue grâce à de multiples grappes de serveurs afin que les données ne soient pas perdues en cas de panne.
- Affectation d'alertes personnalisées en fonction des requêtes et du département informatique en charge.
- Exploitez le flux en direct de vos données au fur et à mesure qu'elles passent par les tuyaux.
- La gestion facile des clusters vous permet d'ajouter plus de puissance et de performance à votre infrastructure de gestion des logs existante.

**Prix :** À partir de 3995$

**Clients :** Toshiba, SONY, Ubisoft, Domino's, Thales


## [Splunk](https://www.splunk.com/fr_fr)
**Description :**

Splunk concentre ses services de gestion des journaux autour des clients d'entreprise qui ont besoin d'outils concis pour rechercher, diagnostiquer et rapporter tout événement entourant les journaux de données. Le logiciel de Splunk est conçu pour supporter le processus d'indexation et de déchiffrement des logs de tout type, qu'ils soient structurés, non structurés ou sophistiqués, basés sur une approche multi-ligne.

**Caractéristiques :**

- Splunk comprend les données machine de tout type ; serveurs, serveurs web, réseaux, échanges, mainframes, dispositifs de sécurité, etc.
- Interface utilisateur flexible pour la recherche et l'analyse des données en temps réel.
- Algorithme de forage pour trouver des anomalies et des modèles familiers dans les fichiers journaux.
- Système de surveillance et d'alerte pour garder un œil sur les événements et les actions importantes.
- Création de rapports visuels à l'aide d'un tableau de bord automatisé.

**Prix :** Gratuit : 500MB/jour -- Splunk Cloud : A partir de $186 -- Splunk Enterprise : à partir de 2 000 $.

**Clients :** Adobe, Coca-Cola, ING, Engie, Symantec, AAA


## [Graylog](https://www.graylog.fr)

**Description :**

Graylog est une plate-forme de gestion de logs gratuite et open-source qui prend en charge la collecte et l'analyse en profondeur des logs. Utilisé par les équipes de la sécurité réseau, des opérations informatiques et de DevOps. Vous pouvez compter sur la capacité de Graylog à discerner les risques potentiels pour la sécurité, vous permet de suivre les règles de conformité, et aide à comprendre la cause fondamentale de toute erreur ou problème particulier que vos applications rencontrent.

**Caractéristiques :**

- Enrichir et analyser les logs à l'aide d'un algorithme de traitement complet.
- Cherchez parmi un nombre illimité de données pour trouver ce dont vous avez besoin.
- Tableaux de bord personnalisés pour la sortie visuelle des données du journal et des requêtes.
- Alertes et déclencheurs personnalisés pour surveiller toute défaillance des données.
- Système de gestion centralisée pour les membres de l'équipe.
- Gestion personnalisée des permissions pour les utilisateurs et leurs rôles.

**Prix :** Gratuit : Open-Source -- Entreprise : à partir de 6 000$/an

**Clients :** King, LinkedIn, Cisco, Petronas


## [ELK](https://www.elastic.co/fr/)
**Description :**

La stack ELK est une solution open source, de la société elastic,composée de trois produits que sont Elasticsearch, Logstash et Kibana, qui permettent de parser, indexer et présenter de gros volumes de données issues de vos logs sous forme de dashbords et de faire des recherches au sein de vos logs comme vous pourriez le faire avec un moteur de recherche.

*Elasticsearch* : est un moteur de recherche et d'analyse distribué, utilisant le format JSON, conçu pour une scalabilité horizontale, 
une fiabilité maximale et une gestion facilitée.

*Logstash* : est un pipeline dynamique de collecte de données, doté d'un large écosystème de plugins et d'une forte synergie avec Elasticsearch.

*Kibana* : Kibana façonne vos données et vous fournit l'interface utilisateur extensible qui vous permet de configurer 
et de gérer la Suite Elastic dans ses moindres aspects.

**Caractéristiques :**

- Il existe pleins d'outils que l'on peut coupler avec ELK
- Permet la gestion d'un grand volume de données
- Hautement personnalisable et dispose de plusieurs fonctions : rapports avancés, capacités de recherche avancée, alertes/notifications, visualisations des données.
- Si on veut profiter de plusieurs fonctionnalités comme les alertes, le monitoring, il est possible d’investir dans le « X-Pack ». Ce pack comporte un ensemble de plugins qui viendront complémenter la stack de base ELK.

**Prix :** Gratuit

**Clients :** Ebay, Netflix, Facebook, Cisco, Orange, Microsoft, Deezer.

## [Microsoft System Center Operations Manager (SCOM)](https://www.microsoft.com/fr-fr/cloud-platform/system-center-pricing)
**Description :**

Microsoft System Center Operations Manager (SCOM) est un composant de la suite logicielle de gestion d'entreprise Microsoft System Center. SCOM déploie, configure, gère et surveille les opérations, les services, les dispositifs et les applications de nombreux systèmes au sein d'une entreprise par le biais d'une console de gestion unique.

**caractéristiques :**

- SCOM est un outil multiplateforme ; il fonctionne sur les systèmes d'exploitation Windows, MacOS et Unix, y compris Linux.
- L'énorme avantage de SCOM est la grande disponibilité des MP et le fait qu'il s'agit d'un produit Microsoft.
- Besoin d'une base de données SQL Server pour stocker toutes ces données
- 
- SCOM n'est disponible que dans le cadre de la suite System Center

**Prix :** Licence de gestion System Center(1323$ - 3607$) -- licence client(62$ à 121$)

**Clients :** 

## Bonus

**Fail2Ban** : Il s’agit d’un IDS/IPS basé sur les logs. Ils sont donc d’une importance capitale ici, car les logs produits vont permettre à Fail2ban de détecter certaines intrusions et ainsi déclencher des actions d’avertissement ou de protection (envoyer un mail, bannir une ip, etc).

**DenyHost :** DenyHost reprend le même principe que Fail2ban, il se base sur les logs dans le fichier /var/log/auth.log des machines Linux pour détecter des tentatives de bruteforce sur le port SSH. Il n’est malheureusement retreint qu’au service SSH.

## Comparatif des solutions

Ce comparatif est issu de l'étude des solutions disponibles au début du projet, le résultat présenté est basé sur des critères d'efficience de l'infrastructure, de simplicité dans la configuration et l'utilisation de la solution et sur les fonctionnalités avancées comme la mise en place d'alerte ou de profilage. On peut classer ce comparatif en trois parties : les solutions historiques, le trio ELK et Graylog.

Les solutions historiques de centralisation des logs sont basées sur syslog-ng ou Rsyslog sur lesquels on vient greffer une interface Web et un stockage dans une base de données type mySQL.
Ce type d'outil n'a pas été retenu, en effet, l'utilisation du format syslog uniquement est un problème majeur. De plus du fait de leurs infrastructures classiques (LAMP, Linux, Apache, Mysql, PHP), ils ont du mal à évoluer et à tenir la charge sur des déploiements importants (volume disque très important, nombre de messages par seconde limité, lenteur de traitement).

### ELK
ELK est une offre composée de 3 outils maintenus par la société Elastic :

- **Elasticsearch** qui est un moteur de recherche orienté document facilement scalable pour répondre à de gros volumes
- **Logstash** qui permet de configurer des flux d’entrées et des flux de sorties (un fichier lu et envoyé vers Elasticsearch par exemple)
- **Kibana** qui est une IHM permettant de consulter les documents d’une base Elasticsearch et d’en sortir des tableaux de bords

Cette solution est très simple à mettre en oeuvre avec des outils qui se prennent en main rapidement. Logstash permet, grâce à de nombreux plugins, d’intégrer des logs en provenance de fichiers, d’un rabbitMQ, d’un syslog, … Kibana permet de créer des tableaux de bords très complet avec de nombreux widgets : camembert, graphique, mappemonde, …

Kibana ne gère cependant pas d’authentification utilisateur. N’importe quel utilisateur peut donc accéder à l’ensemble des logs enregistrés dans le cluster Elasticsearch. La société Elastic propose pour cela un autre outil « Shield » mais qui est payant.

De base, il n’est pas possible de recevoir des alertes sur des conditions précises. Il serait utile par exemple d’être prévenu lorsqu’il y a des tentatives de connexions en erreur sur un serveur ou qu’une base de données n’est plus joignable pour un applicatif. Là aussi, un outil peut être utilisé « Watcher » mais il est soumis à l’achat d’une licence.

Enfin, vous devrez gérer l’épuration des index Elasticsearch par vous même.

### Graylog
Graylog est une solution composée de 3 briques :

- **Elasticsearch** qui est le moteur d’indexation des documents
- **Mongodb** qui est une base NoSQL permettant de stocker la configuration et quelques autres paramètres
- **Graylog-server** et **graylog-web-interface** pour la collecte et la visualisation des logs (ces 2 outils vont fusionner dans la prochaine version)

Graylog permet d’intégrer des logs au format syslog, des messages en provenance d’un Rabbit MQ mais également des documents au format GELF. Le GELF est une sorte de syslog améliorée contenant des meta-datas et qui n’a pas de limite de taille sur la longueur du message. Les logs sont traités par la partie serveur et enregistrées dans Elasticsearch.

La partie IHM vous permet d’effectuer des recherches et de configurer :

- des streams : un stream permet de catégoriser vos logs pour mieux les retrouver. En spécifiant des critères sur la source, le niveau d’erreur, le message, … un log pourra être stocké dans un ou plusieurs streams.
- des alertes : sur chaque stream, il est possible de définir des alertes afin d’être prévenu s’il y a plus de 10 logs d’erreur sur les 5 dernières minutes par exemple. L’alerte peut être de différents types : par mail, par message slack, via un appel de web-services, …
- des tableaux de bords : vous pouvez créer des widgets et les intégrer à des tableaux de bords. A ce jour, il existe beaucoup moins de widgets que sur Kibana, mais des nouveautés arrivent avec la prochaine version de Graylog 
- des utilisateurs et des droits d’accès : l’accès à l’interface web nécessite un login et un mot de passe. L’outil propose également une gestion de droit complète permettant de définir les accès aux streams et dashboard pour chaque utilisateur. Il est même possible de brancher Graylog avec le LDAP de votre entreprise

### Microsoft SCOM(System Center Operations Manager)



### Conclusion
Dans une startup ou une entreprise de petite taille, la stack ELK sera rapidement mise en place et suivant les besoins, il sera possible de la faire évoluer avec les produits proposés par la société Elastic (Watcher, Shield, …) ou de développer ses propres outils pour gérer les alertes par exemple.

Dès lors qu’une entreprise aura besoin d’une authentification pour les utilisateurs et d’alertes pour pouvoir agir rapidement, la solution Graylog sera la plus à même de répondre au besoin.