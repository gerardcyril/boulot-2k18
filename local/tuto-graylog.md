# Présentation

**Graylog** est une plateforme Open Source sous licence **GPLv3** de gestion, centralisation et analyse de logs née en 2010 et développée par la société **Graylog, Inc.** basée à Houston aux Etats-Unis.
Ses principales caractéristiques sont :

- Centralisation des logs émis par un ensemble de serveurs distants
- Stockage des logs dans une base de données Elasticsearch ( en cluster éventuellement)
- Stockage des données de configuration et métadonnées dans une base de données MongoDB
- Moteur Graylog d’analyse et de gestion des logs pourvu d’une interface Web Java
- Configuration de tableaux de bord personnalisés avec widgets
- Authentification par l’intermédiaire d’un annuaire LDAP ou AD
- Affichage statistique sous forme de courbes, camemberts, histogrammes, etc.
- Outil de recherche puissant via une syntaxe optimisée
- Collecte des logs via protocole Syslog/Rsyslog, entre autres formats (GELF, Beats, etc.)
- Gestion des logs venant de serveurs Debian, Ubuntu, CentOS, etc. … ou encore MacOS X et Windows Server (via NXLog et Rsyslog)
- Catégorisation des logs en temps réel par l’intermédiaire de streams

# Schéma logique

![schema](img/tuto/schema-graylog.jpg)

# Pré-requis

## 1. Pré-requis avant réalisation

- Un serveur Debian Stretch 9.4 64 bits fonctionnel (installation basique avec utilitaires usuels du système et service SSH)
- Le serveur doit posséder des ressources conséquentes, notamment du point de vue du CPU (pour la partie Graylog-Server) et de la RAM (pour la partie Elasticsearch).
- Il est fortement conseillé que le serveur Graylog et tous les serveurs émetteurs de logs soient synchronisés via NTP avec la même source de temps
- Packages de base supplémentaires : sudo, vim
- Domaine utilisé : synelia.fr

## 2. Configuration réseau initiale

FQDN : graylog.synelia.fr
Adresse IP : 192.168.11.134
Réseau : 192.168.11.0/24
Passerelle : 192.168.11.1
dns-nameservers : 192.168.11.2

Il faudra biensur modifier les fichiers suivant pour l'intégrer à votre configuration réseau : 
- /etc/network/interfaces
- /etc/hosts
- /etc/resolv.conf

# Réalisation

## 1. Dispositions préalables

**Démarrer avec un système à jour**

~~~
# apt update && apt upgrade
~~~

**Installation de Java 8 et autres paquets pré-requis**

~~~
# apt install apt-transport-https openjdk-8-jre-headless uuid-runtime pwgen curl
~~~


## 2. Installation de MongoDB

MongoDB est utilisé ici pour le stockage des métadonnées et données de configuration du serveur Graylog. Il s’agit d’un SGBD non relationnel et NoSQL sans schéma prédéfini des données où les documents sont enregistrés aux formats BSON et JSON dans des collections (équivalentes des tables).

~~~
# apt install -y mongodb-server
# systemctl daemon-reload
# systemctl enable mongodb.service
# systemctl restart mongodb.service
~~~

## 3. Installation d'Elasticsearch

Elasticsearch est un moteur de recherche libre écrit en Java et utilisant la bibliothèque Apache Lucene, écrite également en Java, permettant l’indexation des données sous forme de documents au format JSON pour procéder à leur analyse et recherche en temps réel.

Les versions 5.x ou 6.x ne sont pas compatible avec la dernière version de Graylog. Il nous faut donc récupérer la dernière version de Elasticsearch qui précède la 5.0.0, à savoir la 2.4.1.

~~~
# wget https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.4.1/elasticsearch-2.4.1.deb
# dpkg -i elasticsearch-2.4.1.deb
~~~

Configuration d'Elasticsearch

~~~
# vi /etc/elasticsearch/elasticsearch.yml
cluster.name: graylog
network.host: localhost

# systemctl daemon-reload
# systemctl enable elasticsearch.service
# systemctl restart elasticsearch.service
~~~

## 4. Installation de Graylog

L’installation du serveur Graylog consiste, dans un premier temps, à installer un nouveau dépôt puis, dans un second temps, à installer le serveur à partir de ce dépôt nouvellement ajouté.

~~~
# wget https://packages.graylog2.org/repo/packages/graylog-2.1-repository_latest.deb
# dpkg -i graylog-2.1-repository_latest.deb
# apt update && apt install graylog-server
~~~

Modification de la configuration :
Nous aurons besoin de deux mots de passe cryptés, que nous allons générer avec deux commandes différentes.

~~~
# pwgen -N 1 -s 96 
MTtPFSMZxAvoLsUiXXauggyJ761hwkGn1ZTN2ovb8wN2tO1LzyeNbaatOrpLukp96p0MxwHQosmMGPborm1YRojnnSORVvr2

# echo -n password | sha256sum
5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8
~~~

Ci-dessus, le mot de passe de l’utilisateur admin sera password.
Il faut ensuite modifiier le fichier /etc/graylog/server/server.conf afin, entre autres, d’ajouter les mots de passe générés.

~~~
# vi /etc/graylog/server/server.conf

password_secret= MTtPFSMZxAvoLsUiXXauggyJ761hwkGn1ZTN2ovb8wN2tO1LzyeNbaatOrpLukp96p0MxwHQosmMGPborm1YRojnnSORVvr2
root_password_sha2= 5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8
root_timezone = Europe/Paris
rest_listen_uri = http://192.168.11.134:9000/api/
web_listen_uri = http://192.168.11.134:9000/
elasticsearch_cluster_name = graylog
elasticsearch_shards = 1
elasticsearch_replicas = 0
~~~

On redémarre le service

~~~
# systemctl daemon-reload
# systemctl enable graylog-server.service
# systemctl restart graylog-server.service
~~~

Verification que le service fonctionne

~~~
systemctl status graylog-server.service
~~~

Si vous voyez un message d'erreur non bloquant relatif au service graylog-server, tapez les commande suivantes pour le corriger
~~~
# apt install execstack
# execstack -c /usr/share/graylog-server/lib/sigar/libsigar-x86-linux.so
~~~

## 5. Vérification des éléments installés

### Interface Web de Graylog

Pour cela, il suffit de se connecter via un navigateur à l’URL suivante : **http://192.168.11.134:9000**
On nous demande alors les identifiants déclarés : **admin** et **password**

![login](img/tuto/tuto-graylog1.png)

L’identification aboutit :

![accueil](img/tuto/tuto-graylog2.png)

Mais on remarque une notification :
Qui nous informe qu’aucun format de message (input) n’a été défini pour la réception des messages :

![notif](img/tuto/tuto-graylog3.png)

Les clients monitorés utilisent rsyslog, nous allons donc définir un input de type Syslog UDP que nous choisissons parmi la liste déroulante :

![input](img/tuto/tuto-graylog4.jpg)

Après avoir cliqué sur Launch new input, la fenêtre suivante apparait, que nous remplissons en spécifiant un port supérieur à 1024 car les ports situés en-dessous de cette valeur sont réservés :

![syslog](img/tuto/tuto-graylog5.PNG)

La valeur 0.0.0.0 signifie une écoute sur toutes les interfaces du serveur Graylog.
Une fois cliqué sur le bouton Save, l’input Syslog UDP se lance automatiquement.

Le serveur est maintenant fonctionnel, mais en allant sur l’onglet Search on remarque qu’aucun message n’est encore reçu, et c’est normal.
Il faut pour cela modifier la configuration du service rsyslog d’un client à monitoré, ce qui sera développé dans la section **Configuration d'un client**.

### Connexion à MongoDB

Comme vu avec l’interface Web de Graylog, tout semble fonctionnel, mais il est toujours mieux de s’en assurer.
La connexion à MongoDB se fait via la commande suivante :

~~~
# mongo localhost:27017/graylog
MongoDB shell version: 3.2.11
connecting to: localhost:27017/graylog
> 
~~~

Connexion apparemment réussie. Vérifions simplement si nous sommes bien connectés à la base de données graylog :

~~~
> db
graylog
~~~

### Connexion à Elasticsearch

La connexion à Elasticsearch se fait par requêtes HTTP donc nous pouvons utiliser soit la commande curl, soit le navigateur Web :

~~~
# curl -X GET 'http://localhost:9200/'
{
  "name" : "Tom Cassidy",
  "cluster_name" : "graylog",
  "cluster_uuid" : "-ZChAKVTQHeBeWmThyAaaA",
  "version" : {
    "number" : "2.4.1",
    "build_hash" : "c67dc32e24162035d18d6fe1e952c4cbcbe79d16",
    "build_timestamp" : "2016-09-27T18:57:55Z",
    "build_snapshot" : false,
    "lucene_version" : "5.5.2"
  },
  "tagline" : "You Know, for Search"
}
~~~

*On peut le faire aussi sur le navigateur web*

Mais pour savoir l’état du cluster, la commande suivante est requise :

~~~
# curl -X GET 'http://localhost:9200/_cluster/health?pretty=true'
{
  "cluster_name" : "graylog",
  "status" : "green",
  "timed_out" : false,
  "number_of_nodes" : 1,
  "number_of_data_nodes" : 1,
  "active_primary_shards" : 1,
  "active_shards" : 1,
  "relocating_shards" : 0,
  "initializing_shards" : 0,
  "unassigned_shards" : 0,
  "delayed_unassigned_shards" : 0,
  "number_of_pending_tasks" : 0,
  "number_of_in_flight_fetch" : 0,
  "task_max_waiting_in_queue_millis" : 0,
  "active_shards_percent_as_number" : 100.0
}
~~~

Le statut green signifie que le cluster est fonctionnel et opérationnel.

Pour afficher l’état des index :

~~~
# curl -X GET 'http://localhost:9200/_cat/indices?v'
health status index     pri rep docs.count docs.deleted store.size pri.store.size
green  open   graylog_0   1   0         0             0       152b           152b
~~~


# Pour aller plus loin

## Configuration d’un client

Comme nous l’avons vu, la page de recherche n’affiche encore aucun message. Nous devons tout d’abord nous assurer que Graylog reçoit bien les messages venant d’un client.
Tout d’abord, on peut vérifier que le port 5140 de l’input Syslog UDP est bien ouvert. Sur le terminal du client, saisir la commande suivante :

~~~
# echo "Salut Graylog, tu reçois bien mes messages?!" | nc -w 1 -u 192.168.11.134 5140
~~~

Si tout va bien, le message devrait apparaitre dans la liste des messages reçus, dans l’onglet Search du serveur Graylog :

![message](img/tuto/tuto-graylog6.png)

Comme la communication est fonctionnelle, nous allons configurer le service Rsyslog de notre client pour qu’il envoie automatiquement les logs en UDP au serveur Graylog.
Pour cela, il faut éditer le fichier de configuration /etc/rsyslog.conf et ajouter la ligne suivante :

~~~
# vi /etc/rsyslog.conf
*.* @192.168.11.134:5140;RSYSLOG_SyslogProtocol23Format
~~~

Si nous redémarrons le service SSH sur le client monitoré, nous devrions observer ses logs sur le serveur Graylog :

![message](img/tuto/tuto-graylog7.PNG)

Ci-dessus, le serveur source (client monitoré) s’appelle client-graylog. Nous pouvons constater que le serveur Graylog reçoit bien les messages du service rsyslog exécuté sur le client monitoré.
Au bout de quelques minutes d’utilisation, on peut voir le nombre de messages reçus augmenter :

~~~
# curl -X GET 'http://localhost:9200/_cat/indices?v'
health status index     pri rep docs.count docs.deleted store.size pri.store.size
green  open   graylog_0   1   0         30            0     65.7kb         65.7kb
~~~

## MongoDB : commandes supplémentaires

Pour afficher les bases de données disponibles :

~~~
> show dbs
graylog 0.125GB
local 0.03125GB
~~~

Pour se connecter explicitement à la base de données graylog :

~~~
> use graylog
switched to db graylog
~~~

Pour afficher les collections (tables) de la base de données graylog :

~~~
> show collections

ou 

> show tables
~~~

Pour afficher les documents de la collection input :

~~~
> db.inputs.find()
~~~

Pas très lisible, pour cela appeler la méthode pretty() :

~~~
> db.inputs.find().pretty()
{
  "_id" : ObjectId("58287c354e6e5f0753a9998b"),
  "creator_user_id" : "admin",
  "configuration" : {
    "expand_structured_data" : false,
    "recv_buffer_size" : 262144,
    "port" : 5140,
    "override_source" : null,
    "force_rdns" : false,
    "allow_override_date" : true,
    "bind_address" : "0.0.0.0",
    "store_full_message" : false
  },
  "name" : "Syslog UDP",
  "created_at" : ISODate("2016-11-13T14:44:05.146Z"),
  "global" : true,
  "type" : "org.graylog2.inputs.syslog.udp.SyslogUDPInput",
  "title" : "Syslog UDP",
  "content_pack" : null
}
~~~

On retrouve bien l’input Syslog UDP précédemment créé.


## Elasticsearch : commandes supplémentaires

Pour afficher la liste des catégories :

~~~
# curl -X GET 'http://localhost:9200/_cat?pretty=true'
~~~

Pour afficher toute la configuration de l’index **graylog_0** :

~~~
# curl -X GET 'http://localhost:9200/graylog_0?pretty=true'
~~~

Equivaut à :

~~~
# curl -X GET 'http://localhost:9200/graylog_0/_aliases,_mappings,_settings,_warmers?pretty=true'
~~~

Pour afficher seulement les paramètres :

~~~
# curl -X GET 'http://localhost:9200/graylog_0/_settings?pretty=true'
~~~

Pour supprimer le contenu de l’index graylog_0 :

~~~
# curl -X DELETE http://localhost:9200/graylog_0/
~~~

# Sources

- [Site officiel Graylog](https://www.graylog.org)
- [Graylog – Documentation officielle](http://docs.graylog.org/en/2.1/index.html)
- [GitHub – Dépôt de la dernière version de Graylog (branche master)](https://github.com/Graylog2/graylog2-server)
- [Site officiel Elasticsearch – Versions disponibles de Elasticsearch](https://www.elastic.co/downloads/past-releases)
- [Site officiel MongoDB – mongo Shell Quick Reference](https://docs.mongodb.com/v3.2/reference/mongo-shell/)
- [Paquets Debian des dépôts Graylog](https://packages.graylog2.org/packages)