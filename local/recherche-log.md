# Les logs

## Qu'est-ce que c'est ? 
Les journaux d'évènements où logs sont des fichiers textes enregistrant de manière chronologique les évènements exécutés par un serveur ou une application informatique.

Un log, événement ou journal, est simplement la notification d’un événement d’une importance plus ou moins élevée envoyée par un service, un système, un élément réseau ou une application se présentant sous la forme d'un fichier texte.
Les journaux d’événement permettent en administration systèmes, réseaux et en sécurité de retracer les actions et la vie d’un système. 

Aujourd’hui, la grande majorité des systèmes, applications et éléments réseau produisent des logs. Dans la quasi-totalité des cas, les logs sont stockés de manière locale sur la machine :

- Les journaux d’événements sous Windows stockent par défaut les logs des OS Windows (Clients et serveurs) via l’observateur d’événement

- Le fameux dossier “/var/log” sous Linux qui stocke l’ensemble des logs systèmes et applicatifs sous Linux via rSysLog (mais pas que, cela dépend des distributions)

## Utilité

Les logs ont un intérêt et une importance cruciale en informatique, car il s’agit là de savoir ce qui s’est passé sur un ensemble d’applications ou systèmes pour pouvoir par exemple :

- Expliquer une erreur, un comportement anormal, un crash sur un service comme un service web

- Retracer la vie d’un utilisateur, d’une application, d’un paquet sur un réseau sur les logs d’un proxy et des éléments réseau par exemple

- Comprendre le fonctionnement d’une application, d’un protocole, d’un système comme les étapes de démarrage d’un service SSH sous Linux

- Être notifié d’un comportement, d’une action, d’une modification telle qu’une extinction ou un démarrage système


## Qu'est ce la centralisation des logs

Lorsque l'on veut consolider un système d'information, l'une des premières choses à faire est une centralisation des logs.
Une centralisation des logs est assez simple pour un système d'informations peu étendues, mais cela s'avère plus complexe lorsque l'on veut l'étendre à un SI de grande taille.

Dans ce cadre, la centralisation des logs consiste simplement à mettre sur un même système, une même plateforme, l’ensemble des logs des systèmes, applications et services des machines environnantes.
On reprend alors le principe de la supervision, qui consiste à centraliser les éléments de surveillance sur une plate-forme unique. On appellera cela un point unique de communication pour l’analyse des logs. 
La centralisation de l’information permettra ici de mener plusieurs opérations qui iront toutes dans le sens de la sécurité et de la meilleure gestion du système d’information.

## Pourquoi une centralisation des logs

Il y a deux principales raisons à une centralisation des logs

- Avoir une vue d’ensemble d’éléments cruciaux à la bonne gestion d’un SI pour y mener des traitements tels que :

	- **IDS/IPS** : Certains systèmes de détection d'intrusion se servent des journaux d’événement pour détecter des comportements anormaux sur le réseau ou sur les systèmes surveillés. Ceux-ci n’utilisent donc pas l’écoute du réseau pour effectuer leur surveillance mais bien les logs.

	- **Recherche/Statistique** : La centralisation des logs va permettre d’effectuer des recherches  très précises sur l’activité de plusieurs systèmes tout en étant sur une machine. Les opérations de recherche et de statistique s’en trouvent donc facilitées, car tout est sur la même plate-forme.

- En cas de crash ou de suppression des logs 

	- **Diagnostiquer un crash :** Il peut être très utile de savoir précisément ce qui s’est passé lorsqu’un système devient complètement injoignable. Si les logs qui permettent de diagnostiquer la panne se trouvent sur ladite machine, ils sont difficilement atteignables. Dans le cas où les logs sont exportés sur une machine dont la disponibilité est assurée, il est alors possible de rapidement récupérer les derniers événements systèmes de la machine endommagée pour diagnostiquer plus facilement.

	- **Garantir la survie des logs :** Une des étapes post-intrusion lors d’une attaque informatique est l’effacement des traces que le pirate aurait pu laisser. Lorsque les logs sont stockés en local, les logs qui sont supprimés sont alors difficilement récupérables, lorsqu’ils sont également envoyés sur une autre machine, il est possible de récupérer des informations. Notons que la suppression des logs peut également arriver dans d’autres contextes plus proches de la vie quotidienne telle que la perte d’un disque, d’une partition dédiée aux logs, la mauvaise manipulation d’un administrateur systèmes pressé

## Aspects juridiques

Le traitement d’éléments de journalisation impose donc le plus souvent le respect des dispositions
de la loi du 6 janvier 1978 :

- la réalisation de formalités préalables auprès de la CNIL (déclaration, autorisation, etc.) ;

- la mise en œuvre d’un niveau de sécurité adapté aux données traitées et aux finalités ;

- la gestion du cycle de vie des éléments de journalisation (processus de création, de conservation, de destruction, etc.) ;

La CNIL ainsi que la jurisprudence 9 ont posé plusieurs principes applicables à la gestion des
éléments de journalisation par des personnes habilitées , par exemple :

- seules des personnes spécifiquement habilitées peuvent accéder aux éléments de journalisation ;

- l’accès doit être strictement limité à la finalité poursuivie, de la manière la moins intrusive possible pour les données à caractère personnel ;

- le personnel habilité est soumis à des obligations de confidentialité particulières et ne doit divulguer une quelconque donnée à caractère personnel que dans des cas limités liés au fonctionnement technique ou à la sécurité des systèmes ou aux intérêts de l’entreprise ;

- le personnel habilité ne doit subir aucune contrainte quant au dévoilement des informations, notamment par son employeur, sauf si la loi en dispose autrement (dans le cadre d’une procédure judiciaire).

| Régime juridique|Public concerné|Type de données(Durée de rétention)|
| :---: | :---: | :---: |
| Hébergeurs | Personnes assurant, même à titre gratuit, le stockage de toute nature fournis par les destinataires de ces services | Données de connexion permettant d'identifier les personnes à l’origine de la création de contenu (1 an) |
| Opérateur de communications électroniques | Toute personne qui, au titre d’une activité professionnelle, offrant au public une connexion permettant une communication en ligne par l’intermédiaire d’un accès à un réseau | - Facturation et paiement des prestations de communications électroniques (1 an) // Recherche et poursuite d’une infraction (1 an) // Protection des SI de l’opération de communication électronique (3 mois) |
| Données à caractère personnel | Responsables de traitement de données à caractère personnel | - Données à caractère personnel (durée de conservation minimale) |
