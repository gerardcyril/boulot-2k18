# Mise en oeuvre

La plupart du temps, la centralisation des logs implique de réfléchir à plusieurs étapes de mise en place. 
Communication entre différents systèmes, parcours des paquets au travers différents réseaux pour arriver à une même cible, catégories/organisation à avoir, informations à exporter et informations à mettre de côté.

## Etape de mise en oeuvre

**1. Faire produire des logs**

Il est important de calibrer correctement la production de logs. Il faut savoir que plus un service, un système ou une application produit de logs, plus celle-ci va consommer des ressources. Il est par exemple généralement déconseillé, sur les services de base de données comme MySQL, de faire produire trop de logs. Ces services ont généralement besoin d’être contactés rapidement et il n’est pas toujours possible de leur fait produire autant de logs qu’on le voudrait, car cela ralentirait les délais de traitements et de calculs. 

Il faut donc veiller à correctement définir ce que l’on souhaite voir, ce que l’on souhaite garder en local et ce que l’on souhaite envoyer sur un serveur de logs distant. Cela demande un certain travail et une vision globale du système d’information, car il est nécessaire d’effectuer cette tâche sur tous les systèmes et les applications qui en ont besoin, tout dépendra donc de la taille de votre système d’information.

**2. Envoyer des logs sur une plateforme commune**

Une fois que l’on sait exactement quoi envoyer et centraliser, il faut maintenant savoir l’envoyer sur la plate-forme destinée à recevoir ces logs. On passe alors également par une phase de réflexion au niveau de l’architecture et de la configuration nécessaire :

- Il faut bien évidemment auparavant avoir configuré et installé le système nécessaire à la bonne réception et au bon stockage de l’ensemble des logs envoyés.

- On va ensuite devoir réfléchir au chemin qu’empruntent nos logs envoyés. En effet, les machines dans le LAN ne vont pas forcément poser problème. En revanche, les machines en DMZ ou sur des sites géographiquement éloignés peuvent poser quelques difficultés. On peut alors se poser différentes questions, est-il judicieux d’ouvrir le port permettant de laisser passer les flux nécessaires de la DMZ au LAN ?

- Il faut également avoir une réflexion sur la configuration et les ressources de la machine de centralisation des logs en elle-même, quelle sera la technologie et l’espace de stockage adéquat et suffisant ? Plusieurs cartes réseau seront-elles nécessaires ? Quelle sécurité mettre en place sur ce serveur ?

**3. Analyser, filtrer et organiser les logs**

Maintenant que nos logs sont arrivés à destination et qu’ils sont dans un endroit sûr, il faut savoir quoi faire de ces journaux d’événements. Quand un log va arriver sur notre serveur de centralisation des logs, il va systématiquement devoir passer par différentes étapes. 

Nous allons d’abord devoir trier ce flux d’information, on peut par exemple créer un répertoire par machine puis un fichier par service, ou alors un fichier par service dans lequel on pourra distinguer la provenance des lignes de logs par le nom de la machine l’ayant produit. L’organisation et le tri des logs se fait de toute manière par le service qui gérera sa réception.

Une fois que nos logs sont dans les bonnes étagères, il va alors être possible de les lire, les examiner, corriger leur position éventuellement pour arriver à l’objectif final de la centralisation des logs : la production d’un outil utile à la bonne gestion du système d’information.

**4. Grapher, Rapporter, Alerter : L’apport en sécurité**

La bonne gestion des logs dans notre serveur de centralisation des logs va permettre de produire différentes choses comme :

- Une fois que nos logs sont correctement organisés et que l’on peut faire des traitements dessus, on pourra par exemple dessiner des graphiques avec des outils spécialisés.

- La centralisation des logs va également permettre leur intégration dans un système de supervision, on va pouvoir plus facilement détecter des comportements ou des logs anormaux par rapport à un comportement standard. 

- Mettre en place des actions de protection et de prévention du système d’information. Avec des logs centralisés, il sera possible d’effectuer une analyse en temps réel des journaux d’événements et cela de façon beaucoup plus simple.

- Il est fréquent, en tant qu’administrateur système, de se faire envoyer des rapports par des scripts qui analysent les logs des machines par exemple. Avec des logs centralisés, on pourra avoir un rapport pour un ensemble/un groupe de machines plutôt que d’avoir un rapport individuel par machine.

