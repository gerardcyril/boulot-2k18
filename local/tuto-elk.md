# Installation ELK

## 0. Pré-requis

Avant de commencer il faut commencer par installer la version 8 ou supérieur de Java. Vous pouvez utiliser la distribution officielle d'Oracle ou utiliser la version open source OpenJDK

~~~
# apt update
# apt install apt-transport-https openjdk-8-jdk
~~~

## 1. Installation et configuration d'Elasticsearch

Importation de la clés & sauvegarde du dépôt

~~~
# wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
# echo "deb http://packages.elastic.co/elasticsearch/2.x/debian stable main" | sudo tee -a /etc/apt/sources.list.d/elasticsearch-2.x.list
~~~

Installation

~~~
# apt update && apt install elasticsearch
# service elasticsearch stop
~~~

Configuration
Avant de démarrer ElasticSearch, nous allons nous assurer que le serveur n'écoute que sur localhost afin d'éviter que n'importe qui puisse accéder à nos données depuis l'extérieur.

~~~
# vi /etc/elasticsearch/elasticsearch.yml
network.host: localhost
~~~

Autre petit détail, si vous êtes sur un système plutôt modeste, vous pouvez changer la quantité de RAM qui est alloué par Java

~~~
# vi /etc/elasticsearch/jvm.options
-Xms2g
-Xmx2g
~~~

On démarre le service et on le met au démarrage

~~~
# service elasticsearch start
# update-rc.d elasticsearch defaults 95 10
~~~

Pour s'assurer que l'installation s'est correctement effectuée et que le service fonctionne correctement on peut appeller l'URL http://localhost:9200

~~~
# curl "localhost:9200"
{
  "name" : "vo6rhbA",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "GC8LhktKRIuyi8lrk3qDtw",
  "version" : {
    "number" : "6.2.3",
    "build_hash" : "c59ff00",
    "build_date" : "2018-03-13T10:06:29.741383Z",
    "build_snapshot" : false,
    "lucene_version" : "7.2.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
~~~

## 2. Installation et configuration de Kibana

Enfin nous allons pouvoir terminer avec la mise en place du dashboard Kibana. Comme d'habitude nous allons passer par les dépôts pour l'installer.

~~~
# wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
# echo "deb http://packages.elastic.co/kibana/4.5/debian stable main" | sudo tee -a /etc/apt/sources.list
~~~


~~~
# apt install kibana
~~~

Nous allons ensuite éditer la configuration afin de ne pas écouter les connexions venant de l'extérieur.

~~~
# vi /etc/kibana/kibana.yml
server.host: "localhost"
~~~

On démarre le service et on le met au démarrage

~~~
# service kibana start
# update-rc.d kibana defaults 96 9
~~~

## 3. Installation de Nginx

Installation

~~~
# apt install nginx apache2-utils
~~~

Utilisation de htpasswd pour créer un utilisateur admin qui pourra accéder à l'interface web de kibana

~~~
# htpasswd -c /etc/nginx/htpasswd.users admin
~~~

*Entrez notre mot de passe*

Configuration de Nginx

~~~
# vi /etc/nginx/sites-available/default
server {
    listen 80;

    server_name kibana.synelia.fr;

    auth_basic "Restricted Access";
    auth_basic_user_file /etc/nginx/htpasswd.users;

    location / {
        proxy_pass http://localhost:5601;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;        
    }
}
~~~

On redémarre le service

~~~
# service nginx restart
~~~

On peut maintenant se connecter à notre service Kibana

~~~
login : admin
password : admin
~~~

## 4. Installation et configuration de Logstash

Maintenant que nous avons installé elasticSearch, il va falloir remplir notre base de données avec les informations provenant de nos logs. Nous allons donc commencer par installer logstash.

~~~
# echo "deb http://packages.elastic.co/logstash/2.3/debian stable main" | sudo tee -a /etc/apt/sources.list

# apt update && apt install logstash
# service logstash stop
~~~

Puisque nous allons utiliser Filebeat pour expédier les journaux de nos serveurs clients à notre serveur ELK, nous devons créer un certificat SSL et une paire de clés.

création des répertoires

~~~
# mkdir -p /etc/pki/tls/certs
# mkdir /etc/pki/tls/private
~~~

Générez maintenant le certificat SSL et la clé privée

~~~
# cd /etc/pki/tls
# openssl req -subj '/CN=ELK_server_fqdn/' -x509 -days 3650 -batch -nodes -newkey rsa:2048 -keyout private/logstash-forwarder.key -out certs/logstash-forwarder.crt
~~~

*Le fichier logstash-forwarder.crt sera copié sur tous les serveurs qui enverront les logs à Logstash mais nous le ferons un peu plus tard. Terminons notre configuration Logstash.*

### Configuration

Création d'un fichier de configuration. La configuration fonctionne en 3 partie :

- La partie input, permet d'indiquer comment les données vont entrer dans le pipeline
- La partie filter va permettre d'indiquer comment parser et extraire les données
- La partie output permet d'indiquer où vont être envoyées les données


~~~
# vi /etc/logstash/conf.d/02-beats-input.conf
input {
  beats {
    port => 5044
    ssl => true
    ssl_certificate => "/etc/pki/tls/certs/logstash-forwarder.crt"
    ssl_key => "/etc/pki/tls/private/logstash-forwarder.key"
  }
}
~~~

~~~
# vi /etc/logstash/conf.d/10-syslog-filter.conf
filter {
  if [type] == "syslog" {
    grok {
      match => { "message" => "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?: %{GREEDYDATA:syslog_message}" }
      add_field => [ "received_at", "%{@timestamp}" ]
      add_field => [ "received_from", "%{host}" ]
    }
    syslog_pri { }
    date {
      match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
    }
  }
}
~~~

~~~
# vi /etc/logstash/conf.d/30-elasticsearch-output.conf
output {
  elasticsearch {
    hosts => ["localhost:9200"]
    sniffing => true
    manage_template => false
    index => "%{[@metadata][beat]}-%{+YYYY.MM.dd}"
    document_type => "%{[@metadata][type]}"
  }
}
~~~


On démarre le service et on le met au démarrage

~~~
# service logstash start
# update-rc.d logstash defaults 96 9
~~~

Si vous souhaitez déboguer votre configuration, il est possible d'utiliser le module de sortie `stdout {}` de tester la configuration avec la commande :

~~~
# /usr/share/logstash/bin/logstash --debug --path.settings /etc/logstash -f /etc/logstash/conf.d/01-local-dev.conf
~~~

A partir de maintenant notre base de données ElasticSearch devrait se remplir en même temps que nos logs d'accès nginx. Pour vérifier que l'index est correctement créé, vous pouvez appeler l'API REST fournie par elasticSearch :

~~~
# curl "localhost:9200/_cat/indices?v"
health status index uuid pri rep docs.count docs.deleted store.size pri.store.size
~~~

## 5. Configuration

### Charger des Dashboards de Kibana

Téléchargement et décompression du paquet

~~~
# curl -L -O https://download.elastic.co/beats/dashboards/beats-dashboards-1.1.0.zip

# apt install unzip

# unzip beats-dashboards-1.1.0.zip
~~~

Charger les exemples de tableaux de bord, les visualisations et les modèles d'index Beats dans Elasticsearch avec la commande suivante

~~~
# cd beats-dashboards-1.1.0
# ./load.sh
~~~

### Charger les modèles dans Elasticsearch

téléchargement 

~~~
# cd ~
# curl -O https://gist.githubusercontent.com/thisismitch/3429023e8438cc25b86c/raw/d8c479e2a1adcea8b1fe86570e42abab0f10f364/filebeat-index-template.json

# curl -XPUT 'http://localhost:9200/_template/filebeat?pretty' -d@filebeat-index-template.json
{
  "acknowledged" : true
}
~~~

## 6. Configurer Filebeat(Ajout des serveurs clients)
Effectuez ces étapes pour chaque serveur Ubuntu ou Debian que vous voulez envoyer des journaux à Logstash sur votre serveur ELK.

### Serveur ELK

~~~
# scp /etc/pki/tls/certs/logstash-forwarder.crt user@ip_client:/tmp
~~~~

### Serveur client

Installation de Filebeat

~~~
# echo "deb https://packages.elastic.co/beats/apt stable main" | tee -a /etc/apt/sources.list.d/beats.list
# wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | apt-key add -

apt-get update
apt-get install filebeat
~~~

Configuration de Filebeat

~~~
vi /etc/filebeat/filebeat.yml
filebeat:
  prospectors:
    -
      paths:
        - /var/log/auth.log
        - /var/log/syslog
      #  - /var/log/*.log

      input_type: log
      
      document_type: syslog

  registry_file: /var/lib/filebeat/registry

output:
  logstash:
    hosts: ["elk_server_private_ip:5044"]
    bulk_max_size: 1024

    tls:
      certificate_authorities: ["/etc/pki/tls/certs/logstash-forwarder.crt"]

shipper:

logging:
  files:
    rotateeverybytes: 10485760 # = 10MB
~~~

On démarre le service et on le met au démarrage

~~~
# service filebeat restart
# update-rc.d filebeat defaults 95 10
~~~

Sur le serveur ELK

~~~
curl -XGET 'http://localhost:9200/filebeat-*/_search?pretty'
~~~

*Si votre sortie affiche 0 résultat total, Elasticsearch ne charge aucun journal sous l'index que vous avez recherché, et vous devriez vérifier votre configuration pour détecter les erreurs. Si vous avez reçu les résultats escomptés, passez à l'étape suivante.*


liens utiles :

https://www.digitalocean.com/community/tutorials/adding-logstash-filters-to-improve-centralized-logging
https://www.digitalocean.com/community/tutorials/how-to-use-kibana-dashboards-and-visualizations