# Cadre du Stage

## Synelia


## Le service d'exploitation


## Mon role dans l'entreprise


# Présentation du projet

## Problématique


## Objectifs


# Etat de l'art

## Les logs

### Qu'est-ce que c'est ? 
Les journaux d'évènements où logs sont des fichiers textes enregistrant de manière chronologique les évènements exécutés par un serveur ou une application informatique.

Un log, événement ou journal, est simplement la notification d’un événement d’une importance plus ou moins élevée envoyée par un service, un système, un élément réseau ou une application se présentant sous la forme d'un fichier texte. 
Les journaux d’événement permettent en administration systèmes, réseaux et en sécurité de retracer les actions et la vie d’un système. 

Aujourd’hui, la grande majorité des systèmes, applications et éléments réseau produisent des logs. Dans la quasi-totalité des cas, les logs sont stockés de manière locale sur la machine : 

- Les journaux d’événements sous Windows stockent par défaut les logs des OS Windows (Clients et serveurs) via l’observateur d’événement ; 
- Le fameux dossier “/var/log” sous Linux qui stocke l’ensemble des logs systèmes et applicatifs sous Linux via rSysLog (mais pas que, cela dépend des distributions).

### Utilité

Les logs ont un intérêt et une importance cruciale en informatique, car il s’agit là de savoir ce qui s’est passé sur un ensemble d’applications ou systèmes pour pouvoir par exemple :

- Expliquer une erreur, un comportement anormal, un crash sur un service comme un service web ; 
- Retracer la vie d’un utilisateur, d’une application, d’un paquet sur un réseau sur les logs d’un proxy et des éléments réseau par exemple ; 
- Comprendre le fonctionnement d’une application, d’un protocole, d’un système comme les étapes de démarrage d’un service SSH sous Linux ; 
- Être notifié d’un comportement, d’une action, d’une modification telle qu’une extinction ou un démarrage système.

### Qu'est ce la centralisation des logs

Lorsque l'on veut consolider un système d'information, l'une des premières choses à faire est une centralisation des logs.
Une centralisation des logs est assez simple pour un système d'informations peu étendues, mais cela s'avère plus complexe lorsque l'on veut l'étendre à un SI de grande taille. 

Dans ce cadre, la centralisation des logs consiste simplement à mettre sur un même système, une même plateforme, l’ensemble des logs des systèmes, applications et services des machines environnantes. 
On reprend alors le principe de la supervision, qui consiste à centraliser les éléments de surveillance sur une plate-forme unique. On appellera cela un point unique de communication pour l’analyse des logs. 
La centralisation de l’information permettra ici de mener plusieurs opérations qui iront toutes dans le sens de la sécurité et de la meilleure gestion du système d’information.

### Pourquoi une centralisation des logs

Il y a deux principales raisons à une centralisation des logs

- Avoir une vue d’ensemble d’éléments cruciaux à la bonne gestion d’un SI pour y mener des traitements tels que :

	- **IDS/IPS** : Certains systèmes de détection d'intrusion se servent des journaux d’événement pour détecter des comportements anormaux sur le réseau ou sur les systèmes surveillés. Ceux-ci n’utilisent donc pas l’écoute du réseau pour effectuer leur surveillance mais bien les logs ; 
	- **Recherche/Statistique** : La centralisation des logs va permettre d’effectuer des recherches  très précises sur l’activité de plusieurs systèmes tout en étant sur une machine. Les opérations de recherche et de statistique s’en trouvent donc facilitées, car tout est sur la même plate-forme. 

- En cas de crash ou de suppression des logs 

	- **Diagnostiquer un crash :** Il peut être très utile de savoir précisément ce qui s’est passé lorsqu’un système devient complètement injoignable. Si les logs qui permettent de diagnostiquer la panne se trouvent sur ladite machine, ils sont difficilement atteignables. Dans le cas où les logs sont exportés sur une machine dont la disponibilité est assurée, il est alors possible de rapidement récupérer les derniers événements systèmes de la machine endommagée pour diagnostiquer plus facilement ; 
	- **Garantir la survie des logs :** Une des étapes post-intrusion lors d’une attaque informatique est l’effacement des traces que le pirate aurait pu laisser. Lorsque les logs sont stockés en local, les logs qui sont supprimés sont alors difficilement récupérables, lorsqu’ils sont également envoyés sur une autre machine, il est possible de récupérer des informations. Notons que la suppression des logs peut également arriver dans d’autres contextes plus proches de la vie quotidienne telle que la perte d’un disque, d’une partition dédiée aux logs, la mauvaise manipulation d’un administrateur systèmes pressé

### Aspects juridiques

Le traitement d’éléments de journalisation impose donc le plus souvent le respect des dispositions
de la loi du 6 janvier 1978 :

- la réalisation de formalités préalables auprès de la CNIL (déclaration, autorisation, etc.) ;
- la mise en œuvre d’un niveau de sécurité adapté aux données traitées et aux finalités ;
- la gestion du cycle de vie des éléments de journalisation (processus de création, de conservation, de destruction, etc.) ;

La CNIL ainsi que la jurisprudence 9 ont posé plusieurs principes applicables à la gestion des
éléments de journalisation par des personnes habilitées , par exemple :

- seules des personnes spécifiquement habilitées peuvent accéder aux éléments de journalisation ;
- l’accès doit être strictement limité à la finalité poursuivie, de la manière la moins intrusive possible pour les données à caractère personnel ;
- le personnel habilité est soumis à des obligations de confidentialité particulières et ne doit divulguer une quelconque donnée à caractère personnel que dans des cas limités liés au fonctionnement technique ou à la sécurité des systèmes ou aux intérêts de l’entreprise ;
- le personnel habilité ne doit subir aucune contrainte quant au dévoilement des informations, notamment par son employeur, sauf si la loi en dispose autrement (dans le cadre d’une procédure judiciaire).

| Régime juridique|Public concerné|Type de données(Durée de rétention)|
| :---: | :---: | :---: |
| Hébergeurs | Personnes assurant, même à titre gratuit, le stockage de toute nature fournis par les destinataires de ces services | Données de connexion permettant d'identifier les personnes à l’origine de la création de contenu (1 an) |
| Opérateur de communications électroniques | Toute personne qui, au titre d’une activité professionnelle, offrant au public une connexion permettant une communication en ligne par l’intermédiaire d’un accès à un réseau | - Facturation et paiement des prestations de communications électroniques (1 an) // Recherche et poursuite d’une infraction (1 an) // Protection des SI de l’opération de communication électronique (3 mois) |
| Données à caractère personnel | Responsables de traitement de données à caractère personnel | - Données à caractère personnel (durée de conservation minimale) |

## Solutions existantes

### Logiciels de centralisation

**[Syslog-ng](https://syslog-ng.com)** 

**Description :**
Syslog est une solution de gestion de logs open-source qui aide les ingénieurs et DevOps à collecter les données de logs à partir d'une grande variété de sources pour les traiter et éventuellement les envoyer vers un outil d'analyse de logs préféré. Avec Syslog, vous pouvez sans effort collecter, diminuer, catégoriser et corréler vos données de logs à partir de votre pile existante et les pousser vers l'avant pour analyse.

**Caractéristiques :**
- Open-source avec une large communauté d'adeptes.
- Évolutivité flexible, quelle que soit la taille de l'infrastructure.
- Prise en charge des plugins pour des fonctionnalités étendues.
- PatternDB pour trouver des modèles dans des journaux de données complexes.
- Les données peuvent être insérées dans des choix de bases de données communes.

**Prix :** Gratuit

**[RsysLog](https://www.rsyslog.com)** 

**Description :**
Rsyslog est un système ultra-rapide conçu pour le traitement des logs. Il offre d'excellents indices de performance, des caractéristiques de sécurité serrées et une conception modulaire pour des modifications personnalisées. Rsyslog s'est développé à partir d'un système de journalisation unique pour pouvoir analyser et trier les logs à partir d'une gamme étendue de sources, qu'il peut ensuite transformer et fournir une sortie à utiliser dans un logiciel dédié à l'analyse des logs.

**Caractéristiques :**

- Facile à implémenter dans des hôtes web communs.
- Vous permet de créer des méthodes d'analyse personnalisées.
- Générateur de configuration en ligne.
- Développement sur mesure disponible à la location.

**Prix :** Gratuit

### Logiciels de traitement des logs

**[Nagios](https://www.nagios.com/products/nagios-log-server/)** 

**Description :**
Nagios fournit une solution complète de gestion et de surveillance des logs qui est basée sur sa plate-forme Nagios Log Server. Avec Nagios, un outil d'analyse de logs leader sur ce marché, vous pouvez augmenter la sécurité de tous vos systèmes, comprendre votre infrastructure réseau et ses événements, et avoir accès à des données claires sur les performances de votre réseau et comment il peut être stabilisé.

**Caractéristiques :**

- Un puissant tableau de bord prêt à l'emploi qui donne aux clients un moyen de filtrer, de rechercher et d'effectuer une analyse complète de toutes les données des journaux entrants.
- Disponibilité étendue grâce à de multiples grappes de serveurs afin que les données ne soient pas perdues en cas de panne.
- Affectation d'alertes personnalisées en fonction des requêtes et du département informatique en charge.
- Exploitez le flux en direct de vos données au fur et à mesure qu'elles passent par les tuyaux.
- La gestion facile des clusters vous permet d'ajouter plus de puissance et de performance à votre infrastructure de gestion des logs existante.

**Prix :** À partir de 3995$

**Clients :** Toshiba, SONY, Ubisoft, Domino's, Thales

**[Splunk](https://www.splunk.com/fr_fr)**

**Description :**
Splunk concentre ses services de gestion des journaux autour des clients d'entreprise qui ont besoin d'outils concis pour rechercher, diagnostiquer et rapporter tout événement entourant les journaux de données. Le logiciel de Splunk est conçu pour supporter le processus d'indexation et de déchiffrement des logs de tout type, qu'ils soient structurés, non structurés ou sophistiqués, basés sur une approche multi-ligne.

**Caractéristiques :**

- Splunk comprend les données machine de tout type ; serveurs, serveurs web, réseaux, échanges, mainframes, dispositifs de sécurité, etc.
- Interface utilisateur flexible pour la recherche et l'analyse des données en temps réel.
- Algorithme de forage pour trouver des anomalies et des modèles familiers dans les fichiers journaux.
- Système de surveillance et d'alerte pour garder un œil sur les événements et les actions importantes.
- Création de rapports visuels à l'aide d'un tableau de bord automatisé.

**Prix :** Gratuit : 500MB/jour -- Splunk Cloud : A partir de $186 -- Splunk Enterprise : à partir de 2 000 $.

**Clients :** Adobe, Coca-Cola, ING, Engie, Symantec, AAA

### Logiciel de centralisation et traitement des logs

**[Graylog](https://www.graylog.fr)** 

**Description :**
Graylog est une plate-forme de gestion de logs gratuite et open-source qui prend en charge la collecte et l'analyse en profondeur des logs. Utilisé par les équipes de la sécurité réseau, des opérations informatiques et de DevOps. Vous pouvez compter sur la capacité de Graylog à discerner les risques potentiels pour la sécurité, vous permet de suivre les règles de conformité, et aide à comprendre la cause fondamentale de toute erreur ou problème particulier que vos applications rencontrent.

**Caractéristiques :**

- Enrichir et analyser les logs à l'aide d'un algorithme de traitement complet.
- Cherchez parmi un nombre illimité de données pour trouver ce dont vous avez besoin.
- Tableaux de bord personnalisés pour la sortie visuelle des données du journal et des requêtes.
- Alertes et déclencheurs personnalisés pour surveiller toute défaillance des données.
- Système de gestion centralisée pour les membres de l'équipe.
- Gestion personnalisée des permissions pour les utilisateurs et leurs rôles.

**Prix :** Gratuit : Open-Source -- Entreprise : à partir de 6 000$/an

**Clients :** King, LinkedIn, Cisco, Petronas

**[ELK](https://www.elastic.co/fr/)**

**Description :**
La stack ELK est une solution open source, de la société elastic,composée de trois produits que sont Elasticsearch, Logstash et Kibana, qui permettent de parser, indexer et présenter de gros volumes de données issues de vos logs sous forme de dashbords et de faire des recherches au sein de vos logs comme vous pourriez le faire avec un moteur de recherche.

*Elasticsearch* : est un moteur de recherche et d'analyse distribué, utilisant le format JSON, conçu pour une scalabilité horizontale, 
une fiabilité maximale et une gestion facilitée. 
*Logstash* : est un pipeline dynamique de collecte de données, doté d'un large écosystème de plugins et d'une forte synergie avec Elasticsearch. 
*Kibana* : Kibana façonne vos données et vous fournit l'interface utilisateur extensible qui vous permet de configurer 
et de gérer la Suite Elastic dans ses moindres aspects.

**Caractéristiques :**

- Il existe pleins d'outils que l'on peut coupler avec ELK
- Permet la gestion d'un grand volume de données
- Hautement personnalisable et dispose de plusieurs fonctions : rapports avancés, capacités de recherche avancée, alertes/notifications, visualisations des données.
- Si on veut profiter de plusieurs fonctionnalités comme les alertes, le monitoring, il est possible d’investir dans le « X-Pack ». Ce pack comporte un ensemble de plugins qui viendront complémenter la stack de base ELK.

**Prix :** Gratuit

**Clients :** Ebay, Netflix, Facebook, Cisco, Orange, Microsoft, Deezer.

**[Microsoft System Center Operations Manager (SCOM)](https://www.microsoft.com/fr-fr/cloud-platform/system-center-pricing)**

**Description :**
Microsoft System Center Operations Manager (SCOM) est un composant de la suite logicielle de gestion d'entreprise Microsoft System Center. SCOM déploie, configure, gère et surveille les opérations, les services, les dispositifs et les applications de nombreux systèmes au sein d'une entreprise par le biais d'une console de gestion unique.

**Caractéristiques :**

- SCOM est un outil multiplateforme ; il fonctionne sur les systèmes d'exploitation Windows, MacOS et Unix, y compris Linux.
- L'énorme avantage de SCOM est la grande disponibilité des MP et le fait qu'il s'agit d'un produit Microsoft.
- Besoin d'une base de données SQL Server pour stocker toutes ces données
- SCOM n'est disponible que dans le cadre de la suite System Center

**Prix :** Licence de gestion System Center(1323$ - 3607$) -- licence client(62$ à 121$)

## Fonctionnement Client/Serveur



## Preuve de concept des solutions retenus



## Choix du client

# Réalisation du projet

## Mise en place du serveur

### Création de la machine virtuelle

### Installation de Graylog

### Configuration de Graylog

### Authentification

## Récupération des logs clients

### Linux

### Windows

### Apache

### Applicatif

## Traitement des logs

### Graphique

### Alertes

### Sauvegarde

# Bilan

## Bilan du projet

## Bilan professionnel

# Annexes