# Présentation 

Rsyslog est un logiciel libre utilisé sur des systèmes d'exploitation de type Unix (Unix, Linux) transférant les messages des journaux d'événements sur un réseau IP. Rsyslog implémente le protocole basique syslog - qui centralise les journaux d'événements, permettant de repérer plus rapidement et efficacement les défaillances d'ordinateurs présents sur un réseau. Il présente la particularité d'en étendre les fonctionnalités en permettant, notamment, de filtrer sur des champs, de filtrer à l'aide d'expressions régulières et l'utilisation du protocole TCP de la couche transport.

# Schéma

![rsyslog](img/schema-rsyslog.PNG)

# Informations sur les logs

## Template(modèle)

Les template sont une caractéristique clé de rsyslog. Ils permettent de spécifier n'importe quel format qu'un utilisateur pourrait vouloir. Ils sont également utilisés pour la génération dynamique des noms de fichiers. Chaque sortie dans rsyslog utilise des templates - ceci est donc hautement personnalisable. Les modèles sont compatibles avec les formats de stock syslogd qui sont codés en dur dans rsyslog.conf.

## Rediriger ou copier selon la facilitie ou la priorité

Sous Linux quand on parle de gestion de logs, les facilites sont des catégories dans lesquelles les logs vont se "ranger" afin de mieux les archiver et les trier. Parmi ces facilites, on retrouve par exemple :

- auth : Utilisé pour des évènements concernant la sécurité ou l'authentification à travers des applications d'accès (type SSH)
- authpriv : Utilisé pour les messages relatifs au contrôle d'accès
- daemon : Utilisé par les différents processus systèmes et d'application
- kern : Utilisé pour les messages concernant le kernel
- mail :  Utilisé pour les évènements des services mail
- user : Facilitie par défaut quand aucune n'est spécifiée
- local7 : Utilisé pour les messages du boot
- * : Désigne toutes les facilities, par soucis de simplicité c'est ce que nous avons spécifié lors de notre première règle de redirection des logs un peu plus haut
- none : Désigne aucune facilites

En plus de ces facilites, nous retrouvons pour chaque facilities un niveau de gravité (appelé Priorité) qui va du plus grave à la simple information :

- 0-Emerg : Urgence, système inutilisable
- 1-Alert : Alerte. Intervention immédiate nécessaire
- 2-Crit : Erreur système critique
- 3-Err : Erreur de fonctionnement
- 4-Warning :  Avertissement
- 5-Notice : Évènement normaux devant être signalé
- 6-Info : Pour information
- 7-Debug : Message de debogage

**Exemples**

Tout les messages critiques et supérieurs concernant les mails

~~~
mail.err 	@192.168.11.132:514
~~~

Tout les logs sur la sécurité ou l'authentification à travers des applications d'accès

~~~
auth.*		@192.168.11.132:514
~~~

On peut également saisir en une ligne plusieurs types de facilities et de priorité, on trouve pas exemple dans le fichier de configuration par défaut des lignes comme celles-ci :

~~~
*.=debug;\
       auth,authpriv.none;\
       news.none;mail.none     -/var/log/debug
*.=info;*.=notice;*.=warn;\
       auth,authpriv.none;\
       cron,daemon.none;\
       mail,news.none          -/var/log/messages
~~~

On voit ici que toutes les priorités debug sont redirigées vers le fichier "/var/log/debug" et que toutes les priorités info,notice et warn seront dans "/var/log/messages". Pour que ces filtres soient redirigés vers le serveur de logs, il suffit de spécifier l'IP du serveur ainsi que son port comme fait plus haut à la place du nom du fichier.

## Rediriger les logs vers un dossier/fichier par host

On peut également, pour faciliter la hiérarchisation et l'archivage de nos logs lorsque l'on a un grand nombre de client Rsyslog utiliser une arborescence avec un dossier/fichier par hôte plutôt que de mettre tous les logs dans le même fichier que le serveur de logs. On va pour cela utiliser une template que nous mettrons après le bloc "RULES" dans le fichier de configuration du serveur :

~~~
$template syslog,"/var/log/clients/%fromhost%/syslog.log"
~~~

On va ensuite appliquer ce template à tous les logs entrants :

~~~
*.* ?syslog
~~~

Il nous suffira ensuite de redémarrer notre service rsyslog puis de générer des logs depuis les clients. On se retrouvera alors avec un dossier "/var/log/clients/" contenant un dossier par IP/nom client et contenant respectivement un fichier "syslog.log" avec les logs de chaque client respectif, ce qui simplifie la recherche d'information dans les logs d'un client précis.

# Mise en place

## Serveur

Pour le serveur, il faut simplement paramétrer le serveur pour qu'il accepte les logs venant de l'extérieur en ouvrant le port adéquat. Pour cela, on va décommenter les deux lignes suivantes :

~~~
# vi /etc/rsyslog.conf

 # provides UDP syslog reception
module(load="imudp")
input(type="imudp" port="514")

 ou 

 # provides TCP syslog reception
module(load="imtcp")
input(type="imtcp" port="514")
~~~

On redémarre le service et on vérifie que le port est ouvert

~~~
# service rsyslog restart
# netstat -nul
~~~

## Client

- **Linux**

Pour le client, on va modifier le même fichier pour rediriger tous les logs sur le serveur, ajouter cette ligne à la fin du fichier :

~~~
# vi /etc/rsyslog.conf
*.*		@192.168.11.132:514  # pour de l'UDP
*.* 	@@192.168.11.132:514 # pour du TCP
~~~

On redémarre le service

~~~
# service rsyslog restart
~~~

*Test, envoie de logs*

coté serveur

~~~
# tail -f /var/log/syslog
~~~

coté client

~~~
# logger -t test "message de test"
~~~

*Et normalement le message de test devrait apparaître coté serveur*

- **Windows**

Après le télécharment du [Client](https://www.rsyslog.com/windows-agent/). 

Tools -> Send Syslog Test message

![test-rsyslog](img/test-rsyslog.PNG)

On va aussi faire un `tail -f /var/log/syslog` sur notre serveur

Maintenant que le test est effectué et que nos logs s'envoient bien, on va configurer rsyslog pour qu'il envoie les logs à notre serveur

![envoie-rsyslog](img/envoie-rsyslog.PNG)

- **Apache**

Dans le fichier /etc/apache2/sites-available/... attaché au vhost.

~~~
# vi /etc/apache2/sites-available/default

ErrorLog "|/usr/bin/logger -t apache -p local6.info"
CustomLog "|/usr/bin/logger -t apache -p local6.info" combined
~~~

On modifie le fichier de configuration de rsyslog pour rediriger les logs

~~~
# vi /etc/rsyslog.conf
local6.* @192.168.11.132:514
~~~

Ces logs arriveront sur le serveur dans /var/log/message et on pourra les identifier avec le mot clés "apache" précisé avec le nom de l'hôte

**Routeur Cisco**

~~~
# ena
# clock set 14:00:00 april 20 2018
# conf t
# service timestamp
# logging 192.168.11.132
# logging facility local5
# logging trap informational
~~~
