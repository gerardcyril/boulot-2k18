

## Conventions de travail

Les programmes directement exécutables (shell, perl, binaire) doivent être stockés dans `bin`.

Les documents et fichiers utilisés pour la gestion ou l'avancement du travail seront stockés dans `local`.

Les documents et documentations produits par les participants dans le cadre du projet doivent être stockés dans `doc`. Ceux récupérés (*i.e* non produites explicitement par les participants du projet) doivent être stockés dans `local`.

Les comptes-rendus seront stockés dans `local` sous la forme `cr-AAAA-MM-JJ` avec `AAAA-MM-JJ` la date de la semaine.

Les supports de présentations des travaux seront stockés dans `local` sous la forme `prez-AAAA-MM-JJ-label` avec `AAAA-MM-JJ` la date de la réunion et `label` un identifiant de la présentation.

On utilise uniquement des fichiers dont le contenu est dans un format ouvert *et* libre (pas de word, powerpoint, OOXML, etc.).

Les fichiers texte sont encodés en **UTF-8** et rien d'autre (pas d'UTF-16 *à la* Mac OS ou Windows-1252 *à la* Windows).

## Outils utilisés pour la gestion du projet

- [GIT](https://git-scm.com)

- [Markdown](https://www.markdownguide.org)

- [Sharepoint](https://products.office.com/fr-fr/sharepoint/)